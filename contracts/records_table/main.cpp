#include <eosiolib/eosio.hpp>
#include <array>

using namespace eosio;

#define HASH_SIZE 32

class records_table : public contract {
  protected:
    typedef uint64_t timestamp_t;
    typedef bytes hash_t;

    //@abi table records i64
    struct record_t {
      timestamp_t ts;
      uint64_t owner;
      hash_t id;

      static uint64_t primary_key(const hash_t& hash) {
        return *(uint64_t*) &hash[0];
      }

      uint64_t primary_key() const { return primary_key(id); }
    };

    EOSLIB_SERIALIZE(record_t, (ts)(owner)(id))

    typedef multi_index<N(records), record_t> records_t;
    records_t records;
  public:
      records_table(account_name owner): contract(owner), records(owner, owner) {}

      /// @abi action
      void put(account_name owner, hash_t document_hash) {
        auto key = record_t::primary_key(document_hash);
        // require_auth(owner);
        records.emplace(owner, [&] (auto& r) {
          r.owner = owner;
          r.ts = publication_time();
          r.id = document_hash;
          print("put owner=", owner, ", key=", key, ", self=", _self);
        });
      }

      /// @abi action
      void remove(hash_t document_hash) {
        auto key = record_t::primary_key(document_hash);
        auto record_it = records.find(key);
        eosio_assert(record_it != records.end(), "record has not been found");
        print("remove owner=", record_it->owner, ", key=", key, ", self=", _self, ", ts=", record_it->ts);
        require_auth(record_it->owner);
        records.erase(record_it);
      }
};

EOSIO_ABI( records_table, (put)(remove) )
