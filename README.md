# Private testnet

## Start private testnet
```
cd chains/local_testnet
docker build -t diplomachain .
docker run --rm -ti -v $(pwd):/workspace -p 8000:8000 -p 8080:8080 -p 3000:3000  diplomachain bash
./start.py
```

## Compile contract
```
cd contracts/records_table
docker run -ti --rm -v $(pwd):/build eosio/eos-dev bash
cd /build
make
exit
```

## Deploy contract
Create account for project
```
make deploy -C contracts/records_table
```


