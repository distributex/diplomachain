#!/usr/bin/env python3
import subprocess
import os
import sys
import logging
import http.client
import time
import platform
import argparse
import json
import os

logging.basicConfig(level=logging.DEBUG, format="%(message)s")
if platform.system() == 'Linux':
    DEFAULT_CONFIG_PATH = '~/.local/share/eosio/'
else:
    DEFAULT_CONFIG_PATH = '~/Library/Application Support/eosio/nodeos/config'
WALLET_PASSPHRASE="PW5KVkBLuhuSyuruprJsoW3yvR6zDFvt2Kz5FgtnyYjKh8asDayK5"

CONFIG_PATH = os.path.expanduser(getattr(os.environ, 'EOS_CONFIG_PATH', DEFAULT_CONFIG_PATH))

def wait(endpoint, path):
        while True:
            try:
                c = http.client.HTTPConnection(endpoint[0], endpoint[1], timeout=3)
                c.request('GET', path)
                res = c.getresponse()
                if res.status == 200:
                    break
            except (ConnectionRefusedError, OSError, TimeoutError) as e:
                logging.info('waiting for %s', endpoint)
                time.sleep(1)
            finally:
                c.close()


def start(args):
    pid = 0
    p = []
    try:
        subprocess.check_call('install -d -m 0755 %s' % CONFIG_PATH, shell=True)
        subprocess.check_call('cp -r nodeos %s' % CONFIG_PATH, shell=True)
        subprocess.check_call('cp -r eosio-wallet ~/', shell=True)

        nodeos_out = open(os.path.join(CONFIG_PATH, 'nodeos.log'), 'w') if args.f else sys.stdout
        nodeos = subprocess.Popen('nodeos --config-dir %s -e -p eosio --plugin eosio::chain_api_plugin --plugin eosio::history_api_plugin --max-irreversible-block-age %d --access-control-allow-origin "*" --access-control-allow-headers "*" --access-control-allow-credentials true' % (CONFIG_PATH, (2**31 - 1)),
            shell=True, stderr=nodeos_out)

        p.append(nodeos)
        wait(('localhost', 8888), '/v1/chain/get_info')

        keosd = subprocess.Popen('keosd', shell=True)
        p.append(keosd)
        wait(('localhost', 8889), '/v1/wallet/list_wallets')

        with open(os.path.join(os.path.dirname(__file__), 'config.json'), 'r') as fd:
            opt = json.load(fd)

        unlock = subprocess.Popen('cleos --wallet-url={wallet_endpoint} wallet unlock'.format(**opt), stdin=subprocess.PIPE, shell=True)
        unlock.communicate(input=WALLET_PASSPHRASE.encode('utf8'))

        subprocess.call('cleos --wallet-url={wallet_endpoint} create account eosio test {pub_key} {pub_key}'.format(**opt), shell=True)
        subprocess.call('cleos --wallet-url={wallet_endpoint} create account eosio diplomachain {pub_key} {pub_key}'.format(**opt), shell=True)

        if args.d:
            pid = os.fork()
            if pid != 0:
                logging.debug("forked to background with pid %d", pid)
                sys.exit(0)

        nodeos.wait()
        keosd.wait()
    except (KeyboardInterrupt, SystemExit) as e:
        logging.info('interrupted')
    finally:
        if pid == 0:
            for process in p:
                process.terminate()
                process.wait()

def start_docker(args):
    docker_image="distributex/diplomachain:latest"
    subprocess.call("docker build . -t %s" % docker_image, shell=True)
    return subprocess.call("docker run -v $(pwd):/workspace -w /workspace --rm -ti -p 8888:8888 -p 8889:8889 %s" % docker_image, shell=True)

def main(args):
    if args.docker:
        return start_docker(args)
    else:
        return start(args)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', help="fork to background", action="store_true")
    parser.add_argument('-f', help="redirect stdout to file", action="store_true")
    parser.add_argument('--docker', help="start in docker container", action="store_true")
    args = parser.parse_args()
    main(args)
