import React, { Component } from 'react';
import { simpleRestClient, Admin, Resource, Delete } from 'admin-on-rest';
import Document from './models/Document';
import './App.css';
import {API_ENDPOINT} from './Store';
import restDecorator from './REST';


class App extends Component {
  render() {
    const restClient = restDecorator(simpleRestClient(API_ENDPOINT));

    return (
      <Admin restClient={restClient}>
        <Resource name="document" list={Document.ListComponent} edit={Document.EditComponent} create={Document.CreateComponent} show={Document.ShowComponent} icon={Document.Icon} remove={Delete} filters={Document.FilterComponent}/>
      </Admin>
    );
  }
}

export default App;
