const EOS = require('eosjs');

const CONTRACT_ACCOUNT = 'diplomachain';
const TABLE = 'records';
const config = require('./config.json');

const EOS_ENDPOINT = config.nodeos_endpoint;
const TESTNET_PRIVATE_KEY = config.priv_key;
const ACTOR_ACCOUNT = config.user;


export class RecordsChainTable {
    async init(actor=ACTOR_ACCOUNT, wif=TESTNET_PRIVATE_KEY, endpoint=EOS_ENDPOINT) {
        const config = {
            keyProvider: [wif],
            httpEndpoint: endpoint,
            broadcast: true,
            sign: true
        };

        this.actor= actor;
        this.eos = EOS(config);
        this.contract = await this.eos.contract(CONTRACT_ACCOUNT);
    }

    async put(digest) {
        console.log('put', digest);
        return await this.contract.put(this.actor, digest, { authorization: this.actor });
    }

    async remove(digest) {
        console.log('remove', digest);
        return await this.contract.remove(digest, { authorization: this.actor });
    }

    async get(digest) {
        const res = await this.eos.getTableRows(true, CONTRACT_ACCOUNT, CONTRACT_ACCOUNT, TABLE, digest);
        console.log('get', digest, res.rows);
        return res.rows[0];
    }

    static decodeName(id) {
        return EOS.modules.format.decodeName(id);
    }
}


export default RecordsChainTable;