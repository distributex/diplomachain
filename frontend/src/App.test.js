import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import {RecordsChainTable} from './Chain';
// import {toHexString, computeDigest} from './Util';
// import config from './config';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

// it('works with contract code', async (done) => {
//   const records = new RecordsChainTable();
//   await records.init(config.user, config.priv_key);

//   const data = new Uint32Array(10);
//   window.crypto.getRandomValues(data);
//   const digest = toHexString(computeDigest(data));
//   await records.put(digest);
//   await records.get(digest);
//   await records.remove(digest);
// });