import {RecordsChainTable} from './Chain';
import config from './config.json';

export const API_ENDPOINT='http://localhost:8000/api/v1';

const store  = {
    state: {
        user: {name: config.user, wif: config.priv_key},
        records: null,
    }
};

export async function initChain() {
    const eos = new RecordsChainTable();
    await eos.init(store.state.user.name, store.state.user.wif);
    store.state.records = eos;
}

//TODO: temporary
initChain().then(()=> console.log('Initialized chain')).catch(err => console.error('Failed to initialize chain:', err));

export default store;