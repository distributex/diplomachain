import store from './Store';
import {computeFileDigest} from './Util.js';
const convertFileToBase64 = async (file) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file.rawFile);
    reader.onload = () => {
        console.log('convertFileToBase64', reader.result);
        return resolve(reader.result)
    };
    reader.onerror = reject;
});

function fileUploadDecorator(restClient) {
    return async (type, resource, params) => {
        if (params.data && params.data.files) {
            /*
                params.data.files[i] = {
                    rawFile: File,
                    data: 'blob:http://url/<id>',
                    name: 'file.pdf',
                    length: '<bytes>'
                }
            */
            const files = [...params.data.files];

            for (let i = 0; i < files.length; i++) {
                const value = files[i];
                if (value.rawFile instanceof File) {
                    const fileContent = await convertFileToBase64(value);
                    files[i] = fileContent;
                }
            }
            params = { ...params, data: {...params.data, files: files, data: files[0]}};
        }
        return await restClient(type, resource, params);
    };
}

function chainDecorator(restClient) {
    return async (type, resource, params) => {
        let blockchain = null;
        if (resource === 'document') {
            const digest = params.id;
            console.log('chainDecorator', digest, type, resource, params);

            switch(type) {
                case 'CREATE':
                    const file = params.data.files[0].rawFile;
                    const newDigest = await computeFileDigest(file);
                    await store.state.records.put(newDigest);
                break;

                case 'DELETE':
                    await store.state.records.remove(digest);
                break;

                case 'UPDATE':
                {
                    const file = resource.data.files[0].rawFile;
                    const newDigest = await computeFileDigest(file);
                    await store.state.records.remove(digest);
                    await store.state.records.put(newDigest);
                }
                break;

                case 'GET_ONE':
                    blockchain = await store.state.records.get(digest);
                break;
                default:
                break;
            }
        }
        let res = await restClient(type, resource, params);

        if (blockchain) {
            res.data.blockchain = blockchain;
        }
        return res;
    };
}

const decorator = (restClient) => {
    const decorators = [fileUploadDecorator, chainDecorator];
    for (const decorator of decorators) {
        restClient = decorator(restClient);
    }
    return restClient;
};

export default decorator;