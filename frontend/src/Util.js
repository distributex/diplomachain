export async function computeFileDigest(file) {
    var reader = new FileReader();
    const promise = new Promise((resolve, reject) => {
        reader.onload = async (event) => {
            return resolve(await computeDigest(event.target.result));
        };
    });

    reader.readAsArrayBuffer(file);
    return await promise;
}

export function toHexString(byteArray) {
    return Array.from(byteArray, function (byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('');
}

export async function computeDigest(data) {
    const hash = await crypto.subtle.digest('SHA-256', data);
    return toHexString(new Uint8Array(hash));
}

export default {};