// in posts.js
import React from 'react';
import { List, Datagrid, Edit, Create, SimpleForm, TextField, TextInput, EditButton, FileField, FileInput, DateInput, SimpleShowLayout, Show } from 'admin-on-rest';
import { ReferenceInput, Filter, SelectInput } from 'admin-on-rest';
import BookIcon from 'material-ui/svg-icons/action/book';
import { Document, Page } from 'react-pdf';
import Chain from '../Chain';

export const Icon = BookIcon;

const API_DOCUMENT_ENDPOINT = 'api/v1/document';

class DocumentField extends React.Component {
    state = {
        numPages: null
    }

    onDocumentLoadSuccess = ({ numPages }) => {
        this.setState({ numPages });
    }

    render() {
        if (this.props.source && this.props.record[this.props.source]) {
            const fileDataURL = this.props.record[this.props.source];
            return (
                <Document file={fileDataURL}
                onLoadSuccess={this.onDocumentLoadSuccess}
                noData="Document is not provided">
                {
                    Array.from(
                    new Array(this.state.numPages),
                    (el, index) => (
                        <Page
                        key={`page_${index + 1}`}
                        pageNumber={index + 1}
                        width={300}
                        />
                    ),
                    )
                }
                </Document>
            );
        } else {
            return (<div></div>);
        }
    }
};

function setRecord(obj, key, newValue) {
    const keys = key.split('.');
    let value = obj;
    if (keys.length > 1) {
        for (const k of keys.slice(0, keys.length - 2)) {
            value = value[k];
        }
    }

    value[keys[keys.length - 1]] = newValue;
}

function getRecord(obj, key) {
    const keys = key.split('.');

    let value = obj;
    for (const k of keys) {
        value = value[k];
    }

    return value;
}

class DecoratableField extends TextField {
    constructor(props) {
        super(props);
        const recordValue = getRecord(props.record, props.source);
        console.log('DecoratableField', props, recordValue);
        const decoratedRecordValue = props.decorator(recordValue);
        setRecord(props.record, props.source, decoratedRecordValue);
    }
};

const ListComponent = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField label="Digest" source="hash" />
            <TextField label="First name" source="metadata.first_name" />
            <TextField label="Last nane" source="metadata.last_name" />
            <TextField label="University" source="metadata.university.name" />
            <TextField label="University department" source="metadata.university.deparment" />
            <TextField label="Speciality code" source="metadata.university.speciality" />
            <TextField label="Date" source="metadata.date" />

            <EditButton basePath={API_DOCUMENT_ENDPOINT} />
        </Datagrid>
    </List>
);

const TitleComponent = ({ record }) => {
    return <span>Document {record.hash}</span>;
};

const ShowComponent = (props) => (
    <Show {...props}>
        <SimpleShowLayout>
            <TextField label="Digest" source="hash" />
            <TextField label="First name" source="metadata.first_name" />
            <TextField label="Last nane" source="metadata.last_name" />
            <TextField label="University" source="metadata.university.name" />
            <TextField label="University faculty" source="metadata.university.faculty" />
            <TextField label="University department" source="metadata.university.deparment" />
            <TextField label="Speciality code" source="metadata.university.speciality" />
            <TextField label="Date" source="metadata.date" />
            <DecoratableField label="Blockchain timestamp" source="blockchain.ts" decorator={(unixTs) => (new Date(unixTs)).toISOString()} />
            <DecoratableField label="Blockchain owner" source="blockchain.owner" decorator={(ownerId) => Chain.decodeName(ownerId) }/>
            <DocumentField label="Document preview" source="data" />
        </SimpleShowLayout>
    </Show>
);


const EditComponent = (props) => (
    <Edit title={<TitleComponent />} {...props}>
        <SimpleForm>
            <TextField label="Digest" source="hash" />

            <TextInput label="First name" source="metadata.first_name" />
            <TextInput label="Last nane" source="metadata.last_name" />
            <TextInput label="University" source="metadata.university.name" />
            <TextInput label="University faculty" source="metadata.university.faculty" />
            <TextInput label="University department" source="metadata.university.deparment" />
            <TextInput label="Speciality code" source="metadata.university.speciality" />
            <DateInput label="Date" source="metadata.date" />

            <DocumentField label="Document preview" source="data" />

            <FileInput source="files" label="Related files" accept="application/pdf">
                <FileField source="data" title="title" />
            </FileInput>
        </SimpleForm>
    </Edit>
);

const CreateComponent = (props) => (
    <Create title="Create a document" {...props}>
        <SimpleForm>
            <TextInput label="First name" source="metadata.first_name" />
            <TextInput label="Last nane" source="metadata.last_name" />
            <TextInput label="University" source="metadata.university.name" />
            <TextInput label="University faculty" source="metadata.university.faculty" />
            <TextInput label="University department" source="metadata.university.deparment" />
            <TextInput label="Speciality code" source="metadata.university.speciality" />
            <DateInput label="Date" source="metadata.date" />

            <FileInput source="files" label="Related files" accept="application/pdf">
                <FileField source="src" title="title" />
            </FileInput>

            {/* <DocumentField label="Document preview" source="files.0.rawFile" /> */}
        </SimpleForm>
    </Create>
);

const FilterComponent = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <ReferenceInput label="Document" source="hash" reference="document">
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);

export default {
    ListComponent,
    EditComponent,
    CreateComponent,
    ShowComponent,
    FilterComponent
};