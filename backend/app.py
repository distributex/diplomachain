#!/usr/bin/env python3
import pymongo
import logging
import sanic
import sys
from sanic import Sanic
from sanic import response as Response
from sanic_cors import CORS
from sanic_auth import Auth
import hashlib
import json
import os
import config
import hashlib
import binascii
import argparse

# console = logging.StreamHandler()
# console.setLevel(logging.DEBUG)
# console.setFormatter("%(asctime)s %(message)s")

# logger = logging.getLogger(__name__)
# logger.addHandler(console)

logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(message)s")
logger = logging

# TODO: check - is it okay that i await here?
def handle_duplicate_error(original_function):
    async def new_function(*args, **kwargs):
        try:
            result = await original_function(*args, **kwargs)
            return result
        except pymongo.errors.DuplicateKeyError as ex:
            logger.warn(ex.details)
            return Response.text('Duplicate hash is not allowed', status=409)
    return new_function

def log_api(decorated_function):
    async def f(request, *args, **kwargs):
        logger.debug('args=%s, files=%s, json=%s', request.args, request.files, request.json)
        return await decorated_function(request, *args, **kwargs)
    return f

def get_data(js_data):
    metadata, base64_data = js_data.split(',')
    return metadata, binascii.a2b_base64(base64_data)

def compute_digest(js_data):
    m = hashlib.sha256()
    _, data = get_data(js_data)
    m.update(data)
    d = m.digest()
    return d

def document_data_to_base64(doc):
    return "{header},{data}".format(header=doc['header'], data=binascii.b2a_base64(doc['data']).decode()).strip()


def main(args):
    client = pymongo.MongoClient(config.MONGO_HOST, config.MONGO_PORT)
    # , username=config.MONGO_USERNAME, password=config.MONGO_PASSWORD)
    db = client[config.MONGO_DB]
    collection = db[config.MONGO_COLLECTION]
    result = collection.create_index([('hash', pymongo.ASCENDING)], unique=True)
    app = Sanic()
    auth = Auth(app)
    CORS(app, expose_headers=["X-Total-Count", "Content-Range"])
    session = {}


    @app.route("/api/v1/document/<hash>", methods=["OPTIONS"])
    async def get_document_options(request, hash):
        return Response.text('', status=200)

    @app.route("/api/v1/document/<hash>", methods=["GET"])
    async def get_document(request, hash):
        doc = collection.find_one({'hash': binascii.a2b_hex(hash)})
        if (doc is None):
            return Response.json({}, status=404)

        data = document_data_to_base64(doc)
        digest = binascii.b2a_hex(doc['hash'])
        logger.debug('get_document(): %s', data)

        return Response.json({
            'metadata': doc['metadata'],
            'data': data,
            'hash': digest,
            'id': digest
        })

    @app.route("/api/v1/document", methods=["OPTIONS"])
    async def get_documents_options(request):
        return Response.text('', status=200)

    @app.route("/api/v1/document", methods=["GET"])
    @log_api
    async def get_documents(request):
        filter_opt = json.loads(request.args['filter'][0]) if 'filter' in request.args else dict()
        range_opt = json.loads(request.args['range'][0]) if 'range' in request.args else [0, 2**32-1]
        count = collection.count_documents({})
        documents = collection.find(filter_opt, limit=range_opt[1], skip=range_opt[0])
        result = list(map(lambda doc: {
            'metadata': doc['metadata'],
            'hash': binascii.b2a_hex(doc['hash']),
            'id': binascii.b2a_hex(doc['hash'])
        }, documents))
        logger.debug('GET response %s', result)

        return Response.json(result, headers={'X-Total-Count': count, 'Content-Range': count})
        # query = request.args.get('q')
        # fields = ['hash','metadata','data']
        # matches = []
        # included_docs = set()
        # for field in fields:
        #     documents = collection.find({field: {'$regex':'.*' + query + '.*'}})
        #     documents = map(lambda doc: {
        #         'metadata': doc['metadata'],
        #         'data': doc['data'],
        #         'hash': doc['hash']
        #     }, documents)
        #     documents= list(documents)
        #     if documents:
        #         matches.extend(
        #             filter(
        #                 lambda doc: doc['hash'] not in included_docs, documents
        #             )
        #         )
        #         for doc in documents:
        #             print(doc['hash'])
        #             included_docs.add(doc['hash'])
        #     print(included_docs)
        # return Response.json(matches)


    @app.route("/api/v1/document", methods=["POST"])
    @handle_duplicate_error
    @log_api
    async def create_document(request):
        digest = compute_digest(request.json['data'])
        data_header, data = get_data(request.json['data'])

        collection.insert_one({
            'metadata': request.json['metadata'],
            'data': data,
            'header': data_header,
            'hash': digest
        })
        return Response.json({'id': binascii.b2a_hex(digest) }, status=200)

    @app.route("/api/v1/document/<hash>", methods=["PUT"])
    @handle_duplicate_error
    async def edit_document(request, hash):
        changes = {
            'metadata': request.json.get('metadata', {})
        }

        if 'data' in request.json:
            data_header, data = get_data(request.json['data'])
            digest = compute_digest(request.json['data'])
            changes['data'] = data
            changes['header'] = data_header
            changes['hash'] = digest

        collection.update_one({'hash': binascii.a2b_hex(hash)}, {'$set': changes}, upsert=True)
        return Response.json({'id': hash}, status=200)


    @app.route("/api/v1/document/<hash>", methods=["DELETE"])
    async def delete_document(request, hash):
        res = collection.delete_one({'hash': binascii.a2b_hex(hash)})
        return Response.json({ 'id': hash }, status=200 if res.deleted_count > 0 else 404)

    if args.d:
        fpid = os.fork()
        if fpid != 0:
            logger.info('forked to backgroudn with pid %d', fpid)
            return 0

    app.run(host='0.0.0.0', port=8000)
    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', help='fork to the background', action='store_true')
    args = parser.parse_args()
    sys.exit(main(args))
