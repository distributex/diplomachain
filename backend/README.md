# Backend for diplomachain
## mongodb + sanic

# Prerequisites
python3

pip3

# Installation
```
pip3 install -r requirements.py

cp example_config.py config.py
```
edit config.py file if needed

# Running

python3 app.py

# Methods

### /api/v1/get_document/{hash}
### /api/v1/put_document/
### /api/v1/edit_document/
### /api/v1/find_document/